<?php
namespace R3C\Wordpress\EventDispatcher;

use Symfony\Component\EventDispatcher\EventDispatcher as SymfonyEventDispatcher;
use Symfony\Component\EventDispatcher\Event;

class EventDispatcher
{
    public static $dispatcher;

	public static function addListener($function, $event = 'init')
    {
        if (self::$dispatcher == null) {
            self::$dispatcher = new SymfonyEventDispatcher();
        }

        self::$dispatcher->addListener($event, $function);
	}

    public static function dispatch($event)
    {
        if ( ! self::$dispatcher == null) {
            self::$dispatcher->dispatch($event);
        }
    }

    public static function dispatchInit()
    {
        self::dispatch('init');
    }

    public static function init()
    {
        global $wp_filter;

        if ( ! isset($wp_filter)) {
            $wp_filter = [];
        }

        if ( ! array_key_exists('init', $wp_filter)) {
            $wp_filter['init'] = [];
        }

        if ( ! array_key_exists('0', $wp_filter['init'])) {
            $wp_filter['init']['0'] = [];
        }

        $wp_filter['init']['0']['r3c_eventdispatcher_init'] = [
            'function' => 'R3C\Wordpress\EventDispatcher\EventDispatcher::dispatchInit',
            'accepted_args' => 1
        ];
    }
}